import React, {Component} from 'react';
import { connect } from 'react-redux';
import { selectSong } from '../actions';

class SongList extends Component {
    constructor(props) {
        super(props);
        this.renderSongs = this.renderSongs.bind(this);
        this.handleSelect = this.handleSelect.bind(this);

    }

    handleSelect(song) {
        this.props.selectSong(song);
    }

    renderSongs() {
        const {songs} = this.props;
        return songs.map((song, i) => {
            return <div key={song.title} className='item'>
                <div className='right floated content'>
                    <button className='ui button primary' onClick={() => this.handleSelect(song)}>
                        Select
                    </button>
                </div>

                <div className='content'>
                    {song.title}
                </div>
            </div>
        })
    }

    render() {
        console.log('songs = ', this.props.songs);
        return <div className='ui divided list'>
            {this.renderSongs()}
        </div>
    }
}

const mapStateToProps = (state, props) => {
    return {
        songs: state.songs
    }
};

export default connect(mapStateToProps, {
    selectSong
})(SongList);