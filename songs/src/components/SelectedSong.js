import React, {Component} from 'react';
import { connect } from 'react-redux';

class SelectedSong extends Component {
    constructor(props) {
        super(props);
        this.renderSongs = this.renderSongs.bind(this);
    }

    renderSongs() {
        const {songs} = this.props;
        return songs.map((song, i) => {
            return <div key={song.title} className='item'>
                <div className='right floated content'>
                    <button className='ui button primary'>
                        Select
                    </button>
                </div>

                <div className='content'>
                    {song.title}
                </div>
            </div>
        })
    }

    render() {
        const {song} = this.props;
        if (song) {
            return <div className='ui divided list'>
                <div>
                    Title: {song.title}
                    Duration: {song.duration}
                </div>
            </div>
        } else {
            return <div>
                <h1>Select song !</h1>
            </div>
        }
    }
}

const mapStateToProps = (state, props) => {
    return {
        song: state.selectedSong
    }
};

export default connect(mapStateToProps)(SelectedSong);